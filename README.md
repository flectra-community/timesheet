# Flectra Community / timesheet

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_employee_product](hr_employee_product/) | 2.0.1.0.0| Product is an employee
[hr_timesheet_task_stage](hr_timesheet_task_stage/) | 2.0.1.0.0| Open/Close task from corresponding Task Log entry
[hr_timesheet_sheet_autodraft](hr_timesheet_sheet_autodraft/) | 2.0.1.0.0| Automatically draft a Timesheet Sheet for every time entry that does not have a relevant Timesheet Sheet existing.
[hr_timesheet_sheet_policy_department_manager](hr_timesheet_sheet_policy_department_manager/) | 2.0.1.0.0| Allows setting Department Manager as Reviewer
[sale_timesheet_order_line_sync](sale_timesheet_order_line_sync/) | 2.0.1.0.0| Propagate task order line in not invoiced timesheet lines
[hr_timesheet_time_type](hr_timesheet_time_type/) | 2.0.1.0.0| Ability to add time type in timesheet lines.
[hr_timesheet_sheet](hr_timesheet_sheet/) | 2.0.1.1.0| Timesheet Sheets, Activities
[hr_timesheet_task_required](hr_timesheet_task_required/) | 2.0.1.0.0|         Set task on timesheet as a mandatory field
[hr_timesheet_activity_begin_end](hr_timesheet_activity_begin_end/) | 2.0.1.0.0| Timesheet Activities - Begin/End Hours
[hr_timesheet_analysis](hr_timesheet_analysis/) | 2.0.1.0.0| Analyze tracked time in Graph views
[crm_timesheet](crm_timesheet/) | 2.0.1.0.0| CRM Timesheet
[hr_timesheet_sheet_policy_project_manager](hr_timesheet_sheet_policy_project_manager/) | 2.0.1.0.0| Allows setting Project Manager as Reviewer
[hr_timesheet_report](hr_timesheet_report/) | 2.0.1.0.0| Generate Timesheet Report from Task Logs
[hr_timesheet_sheet_attendance](hr_timesheet_sheet_attendance/) | 2.0.1.0.0| HR Timesheet Sheet Attendance
[hr_utilization_analysis](hr_utilization_analysis/) | 2.0.1.0.0| View Utilization Analysis from Task Logs.
[sale_timesheet_rounded](sale_timesheet_rounded/) | 2.0.1.0.1| Round timesheet entries amount based on project settings.
[hr_timesheet_task_domain](hr_timesheet_task_domain/) | 2.0.1.0.0| Limit task selection to tasks on currently-selected project


